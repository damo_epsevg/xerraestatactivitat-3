package edu.upc.lapps.demoMostraBundle;


import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class DemoMostraBundle extends XerraEstatActivitat {

    String estat;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_mostra_bundle);
        inicialitza();
    }

    private void inicialitza() {
        final EditText text;
        Button boto;
        boto = (Button) findViewById(R.id.boto);
        text = (EditText) findViewById(R.id.text);

        boto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gravarEstat(text.getText().toString());
            }
        });


    }

    private void gravarEstat(String text) {
        estat = text;

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_demo_mostra_bundle, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
             savedInstanceState.putString("Estat", estat);
            super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
            estat = savedInstanceState.getString("Estat");
            super.onRestoreInstanceState(savedInstanceState);
    }

}
